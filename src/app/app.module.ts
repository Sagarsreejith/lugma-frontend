import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LandingPageComponent } from './component/landing-page/landing-page.component';

import { 
	Direction,
	IgxCarouselComponent,
	IgxCarouselModule,
	IgxLinearProgressBarComponent,
	IgxProgressBarModule,
	IgxSliderModule
 } from "igniteui-angular";
@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    IgxCarouselModule,
		IgxProgressBarModule,
		IgxSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
