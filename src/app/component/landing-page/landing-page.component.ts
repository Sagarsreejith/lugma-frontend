import { Component, OnInit, ViewChild } from '@angular/core';
import { IgxCarouselComponent, IgxLinearProgressBarComponent } from 'igniteui-angular';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  @ViewChild("carousel", { static: true }) public carousel: IgxCarouselComponent;
  @ViewChild("linearbar", { static: true }) public linearbar: IgxLinearProgressBarComponent;

  public slides: any[] = [];
  public loop = true;
  public pause = false;
  public total: number;
  public current: number;

  constructor() { }

  public ngOnInit() {
    this.addNewSlide();
    this.carousel.stop();
    this.total = this.slides.length;
    this.current = this.carousel.current;
  }

  public addNewSlide() {
    this.slides.push(
      {
        description: "30+ Material-based Angular components to code speedy web apps faster.",
        heading: "Discover your meal by location",
        image: "http://lugma-byte.com/lugma_design/assets/images/01.png"
      },
      {
        description: "A complete JavaScript UI library empowering you to build data-rich responsive web apps.",
        heading: "Follow your friends",
        image: "http://lugma-byte.com/lugma_design/assets/images/02.png"
      },
      {
        description: "Build full-featured business apps with the most versatile set of ASP.NET AJAX UI controls",
        heading: "Get lateset updates on Promotions and Events",
        image: "http://lugma-byte.com/lugma_design/assets/images/03.png"
      },
      {
        description: "Build full-featured business apps with the most versatile set of ASP.NET AJAX UI controls",
        heading: "Get lateset updates on Promotions and Events",
        image: "http://lugma-byte.com/lugma_design/assets/images/04.png"
      },
      {
        description: "30+ Material-based Angular components to code speedy web apps faster.",
        heading: "Explore the right place with Gallery and Menu",
        image: "http://lugma-byte.com/lugma_design/assets/images/05.png"
      },
      {
        description: "A complete JavaScript UI library empowering you to build data-rich responsive web apps.",
        heading: "Give your opinions and Reviews &amp; Rating",
        image: "http://lugma-byte.com/lugma_design/assets/images/06.png"
      }
    );
  }

  public onSlideChanged(carousel: IgxCarouselComponent) {
    this.current = carousel.current + 1;
    this.linearbar.value = carousel.current + 1;
  }
}